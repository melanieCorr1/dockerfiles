ARG BASE_IMAGE

FROM "$BASE_IMAGE"

USER ${METAPREDICTOR_USER}

WORKDIR metapredictor2
ENV MP2_SETTINGS=/home/metapredictor/mp2_config.py
ENV FLASK_APP=consumer.py
ENV PYTHONPATH=.
CMD ["flask", "consumer-command", "loop"]
