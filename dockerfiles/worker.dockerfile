ARG BASE_IMAGE

FROM "$BASE_IMAGE"

ARG DTU_IMAGE

USER root
RUN apt update && apt install -y perl tcsh gawk

USER ${METAPREDICTOR_USER}
COPY --from="$DTU_IMAGE" /opt/dtu /opt/dtu


WORKDIR metapredictor2
ENV MP2_SETTINGS=/home/metapredictor/mp2_config.py
ENV FLASK_APP=consumer.py
ENV PYTHONPATH=.
CMD ["flask", "consumer-job", "loop"]
