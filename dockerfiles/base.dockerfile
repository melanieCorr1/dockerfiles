# Metapredictor dockerfile
FROM python:3.7-slim-buster

# Settings
ARG GIT_BRANCH
ARG PROXY_URL
#ENV GIT_BRANCH=test
ENV METAPREDICTOR_USER=metapredictor

ENV "http_proxy"="$PROXY_URL"
RUN echo "$PROXY_URL"
ENV "https_proxy"=${http_proxy}
ENV "HTTP_PROXY"=${http_proxy}
ENV "HTTPS_PROXY"=${http_proxy}
ENV "no_proxy"=".sanofi.com,.pharma.aventis.com,web"
ENV "NO_PROXY"=${no_proxy}

# SSL issue fix
# See https://stackoverflow.com/a/62158260
RUN sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1.0/' /etc/ssl/openssl.cnf

# Needed binaries
RUN apt update && apt install -y git && rm -rf /var/lib/apt/lists/*

# Let's go !
RUN useradd --create-home ${METAPREDICTOR_USER}
WORKDIR /home/${METAPREDICTOR_USER}
USER ${METAPREDICTOR_USER}

# Virtual env, following https://pythonspeed.com/articles/activate-virtualenv-dockerfile/ tips
ENV VIRTUAL_ENV=/home/${METAPREDICTOR_USER}/venv
RUN python3 -m venv ${VIRTUAL_ENV}
ENV PATH="${VIRTUAL_ENV}/bin:$PATH"

# Install python base requirements
COPY --chown=${METAPREDICTOR_USER} files/requirements.txt requirements.txt
COPY files/pip.conf pip.conf
ENV PIP_CONFIG_FILE pip.conf

RUN pip3 install -U pip
RUN pip3 install -v -r requirements.txt

# Install eBio packages, now done in requirements.txt
# RUN pip3 install --pre -v anarci eBiology-mhcii-metapredictor eBiology-auth eBiology-immuno-align-scorer eBiology-web-user

# Config files
COPY --chown=${METAPREDICTOR_USER} files/mp2_config.py mp2_config.py

# Clone metapredictor code
RUN git config --global http.sslVerify false
RUN git clone https://metapredictor-deploy:6G9X2yK2DyBAtEm1kZf7@emea-aws-gitlab.sanofi.com:3001/largemolecule/metapredictor/immunogenicity/metapredictor2.git -b "$GIT_BRANCH"

# Patches
WORKDIR metapredictor2
COPY files/test_server.patch .
COPY files/test_server_2.patch .
#COPY files/12_hours.patch .
#COPY files/stop_loop_after_prediction_v2.patch .

RUN git apply --ignore-whitespace test_server.patch
RUN git apply --ignore-whitespace test_server_2.patch
#RUN git apply --ignore-whitespace 12_hours.patch
#RUN git apply --ignore-whitespace stop_loop_after_prediction_v2.patch

WORKDIR ..
