METADB = "postgresql://meta:meta@metadb/metadb"
PREDDB = "/opt/dtu/predictors.ini"
USERSDB = "postgresql://user:user@userdb/userdb"
NINEMERDB = "postgresql://ninemer:ninemer@ninemerdb/ninemerdb"
API_SECRET = "MGMwY2Q3NDQxNWJh"
API_URL = "http://web:5000" # Should reflect pod name
REDIS_HOST = "redis"
REDIS_PORT = 6379
